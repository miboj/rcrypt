## Release summary

rcrypt depends on GnuPG (https://gnupg.org/). If GPG is not installed, then `.onLoad` will produce an error that instructs the user to install GPG.

## Test environments

- local ubuntu 14.04, R 3.2.2
- local Windows 10, R 3.2.2

## R CMD check results

There were no ERRORs, WARNINGs, or NOTEs.

## Downstream dependencies

There are currently no downstream dependencies for this package
